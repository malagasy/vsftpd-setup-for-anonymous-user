# vsftp setup with Anonymous user for SFTP servers

SSH File Transfer Protocol Server (vsftpd) for Anonymous user configuration on Raspbian 10

## Getting Started

These instructions will help you to set up an Anomynous user that you can use on your local network for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

The software is installed and tested with these settings:

```
Raspberry Pi 2 Model B Rev 1.1
Raspbian GNU/Linux 10.6
openssh-server 1:7.9p1-10+deb10u2
vsftpd 3.0.3-12
```

Enable SSH on the Raspberry Pi 

```
sudo systemctl enable ssh
sudo systemctl start ssh
```

Check the SSH service status

```
sudo systemctl status ssh
```

### Installing

A step by step series of examples that tell you how to get a development env running

Update your system 

```
sudo apt update
sudo apt upgrade
```

Install vsftpd

```
sudo systemctl status vsftpd
```

The anonymous user name would be "ftp", and will belong to a group with the same name

```
$ grep ftp /etc/passwd
ftp:x:110:116:ftp daemon,,,:/srv/ftp:/usr/sbin/nologin
```

Create a folder name called "incoming", that is dedicated to that user only

```
sudo mkdir /srv/ftp/incoming
sudo chown -R ftp:ftp /srv/ftp/incoming
```

Set a password to the Anonymous user

```
sudo passwd ftp
```

Take a backup of the vsftp configuration file

```
sudo mv /etc/vsftpd.conf /etc/vsftpd.conf.bak
```

Download and replace the current vsftp configuration file with the [one](vsftpd.conf) from this project

Restart the vsftp service and check its status

```
sudo systemctl restart vsftpd 
sudo systemctl status vsftpd

or 

sudo service vsftpd restart
sudo service vsftpd status
```

## Chroot jail the Anonymous user

Override the [/etc/ssh/sshd_config](sshd_config) file's default settings to match the below

```
# override default of no subsystems
#Subsystem	sftp	/usr/lib/openssh/sftp-server
Subsystem	sftp	internal-sftp

# Example of overriding settings on a per-user basis
#Match User anoncvs
#	X11Forwarding no
#	AllowTcpForwarding no
#	PermitTTY no
#	ForceCommand cvs server
#Match User ftp
Match Group ftp
    X11Forwarding no
    AllowTCPForwarding no
    AllowAgentForwarding no
    ForceCommand internal-sftp

# Specify the chroot jail directory
ChrootDirectory /srv/ftp/
```

Restart the sshd service and check its status

```
sudo systemctl restart sshd 
sudo systemctl status sshd

or 

sudo service sshd restart 
sudo service sshd status
```

## Running the tests

Access the SFTP folder with the "ftp" user credentials

### Windows Client: Windows 10

Open WinSCP

```
Host name: <sftp_IP-ADDRESS_or_FQDN>
Port number: 22
User name: ftp
Password: <ftp_user_password>
Protocol: SFTP
```

### Linux Client: Ubuntu 20.04.1 LTS (XFCE 4.14)

Install the missing packages to allow Thunar to access any shared resources on a local network (ftp, smb, etc.)

```
sudo apt install gvfs-fuse gvfs-backends
```

Open Thunar with the SFTP server IP or FQDN

```
sftp://<sftp_IP-ADDRESS_or_FQDN>/incoming
```

### Tests with the Anonymous user

Verify that the Anonymous user is granted full access, but only into its local folder /srv/ftp/incoming

Access to any other folders should be limited (/srv/ftp) or denied

SSH access to the sever is not allowed, and should fail

```
ssh ftp@<sftp_IP-ADDRESS_or_FQDN>
```

## Deployment

Anonymous user should have restricted permissions while accessing the SFTP server (e.g. download only)

**/!\ Virtual Users should be used instead. It is more secure than using an existing User with an entry in /etc/passwd and /etc/shadow files**

## License

This project is licensed under the GPLv2 License - see the [LICENSE](LICENSE) file for details
